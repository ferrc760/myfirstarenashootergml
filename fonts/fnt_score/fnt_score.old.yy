{
    "id": "d483cf49-5cb5-4ac6-b175-e3c07380dd4a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3d5d479a-1f9c-4cb7-a85c-966238076e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9fc3da42-8768-4af9-b5ca-9ca6f348f517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 239,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bb1a05f2-6be1-4992-a7fe-3e662a4be909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1b0e8487-c1a4-4bb5-b1c0-69f3355ed838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 213,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7f268702-7b7f-47ca-b238-aa5245507a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3e184bd4-7656-4fdc-a2fd-abc1fa765caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 185,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e9dace2b-d698-4ef6-8464-d79468ff7697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 172,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c72d5f77-8ff5-4a25-9594-a7998757546b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 167,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ff3edea8-e103-4335-b610-d5718b49e828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 159,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ffffcee6-8505-4176-8a1d-25ee9078a869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 151,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "115beaf8-2f0f-4fac-9f9e-dbb3fa8b89f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 244,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b8622599-8656-41c0-b497-5d81046a60e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 141,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fad11829-ea25-4b2d-b141-a8fe24a58caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5158c567-572e-482d-afda-fcb733f3345e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 113,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c212aaf9-5e80-480a-a4ef-537c9cb42c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 108,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8d9c9017-2ca0-4149-92fe-e1330ebaf82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "dce14727-06d5-4d2e-a52b-4c01a9db64cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c298840a-d433-48fa-9c19-cbb6960f964e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 77,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d64b17fb-1db7-4765-865c-5f2fe4be3e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 66,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "25997fd9-cc90-4bcb-90ac-1577297319b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 55,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "360761f2-caac-46e5-8e46-9ef4300fceb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8082eaf2-4fce-401c-b0a0-54b093007b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "57fe72cd-7993-4262-9627-49ffca902700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ed3b41f3-fca6-4184-a75c-be8768dc53c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cb847b72-065f-4b5b-b1b2-7ee70ef6a668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9367b059-8cd8-4619-b82e-4132da4e8843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 22,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d6e632a5-3761-4a84-be37-718863be6c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 17,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2136145e-b584-49cd-b882-89bdcef6a0e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 11,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "06e94eaf-9be3-45a0-ae46-f413cd153876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0595a5a1-861f-4962-b093-bc1d1aecd8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 238,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e06884bb-3dec-4eee-bd58-d586b56fd48e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 229,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9b508a14-916b-4b83-b5f1-357a36cb66a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "80c2fe5d-0475-4998-a300-4d8f6025d1be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a0a1cb23-2d9d-403d-bca5-13e45cbe4af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 188,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f6e23fc8-e001-44fa-ab17-331cad81f775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b7981371-09d7-472e-bda3-bcbc68d0a4d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 165,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b271ed7b-f5f2-4599-846b-a9b7746c7f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 152,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "de18a283-3d36-4da0-beb1-cd0cb1c935b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 140,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6f18cfa6-0215-4639-8fc5-48cb9da2d2cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 128,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "63e96d69-e9e0-448e-8dd9-c77046736296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 115,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a3551fcf-2e5e-43c2-91bf-5bc538820e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0f9b77dc-4348-4065-b1d7-d7d52520a3a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0a816c8f-d9ef-4490-b2ec-2bc4833194a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1919d9df-a89f-459b-b782-f59bcdcda35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 65,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8c822b90-ca02-405f-bc5b-064ff137a401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fd927d5c-17d8-4846-85e2-23fbc58fb5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 38,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "11f74d51-7029-4571-8e76-b961102da863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 28,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1b00f20d-2298-4e71-a52c-e6efeaf001c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f8132f89-e8f0-43e6-aa40-ab170d74dd62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9bbc9adb-844c-4d80-ae03-87b77bba1ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 9,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "64b2be49-7f2e-4480-be1e-68c9873180e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6b45fb21-4624-45fb-9b4a-68145ee1e2ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "409e57e6-0e79-49e4-a6f6-184a7adff3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1795a570-a900-4295-be0e-4a9a951ca281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c30c78a8-891f-4cce-92c6-64e7115e7894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "772ff115-4f42-4cb5-a68d-dc0b2c90bb34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c2ccc409-824f-45c7-846e-00f6d545e65f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d7a3d46d-92f5-490c-ac70-60b629904cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "931a7f35-e8fa-47b3-8173-da0aad7fc818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "86ee0633-bb49-453a-8695-533bb8ccfb5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "576a912a-f530-4b28-a671-c99e3a2f80a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b2006097-7332-4be4-bedf-38046ef85ad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "97828ad1-e2c8-4bd9-92f5-6686729affad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "254bfec4-f96c-408b-8406-9c850ee9b4de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "389875f6-0712-48e5-bc37-578156e65aaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6f8e6019-8c3f-4f2e-8af5-abd46a85ca56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "df94cf80-6451-4684-83e4-703f6a999b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3994be43-ce0f-48e5-ac61-f19401d67d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "336f7370-07c9-4d00-8cf3-52a7e6e8dfac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "803be613-a683-4618-a970-37620587a476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8f37ee70-175c-4764-bf62-47b0956fa1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "beb3ca17-2d5d-404c-9787-149348efa33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 25,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ad6ffa37-322a-427e-9442-240e76e50a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4958896f-da63-49ba-8a2b-79087e3f4b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "43b6a3ac-94fb-4887-a82b-b93914fb9d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 235,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "aec3e4c7-c00f-4401-97d5-1e211141da9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 224,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b72819fc-4cd8-47b4-9b2d-1f3ab8ca2e39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 219,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "693bbfbf-7ac5-4d7b-8203-0766fd70e163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 204,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "fd95989d-a537-43db-834c-1a0a0396286e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 193,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fd711da9-bda6-4f42-8d8d-58582ecc95b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 183,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "498e9e61-4349-4409-9ca9-cb7cc0ef5804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 172,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2216ed27-983f-4954-b9e0-80a011aeff2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 162,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7b71f3ce-b191-470d-9240-4865a3b35227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 152,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7b68747d-6630-4f4b-836a-7745ba48ae6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 244,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8f6d2f5c-774d-42ef-a705-68ca9087b389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 142,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "662656aa-6979-401d-90fc-54adb3fbc092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 121,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0f4fe5cf-9513-4e0c-bb76-1b8d228b9a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 111,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e47d402c-15e2-4a57-8c46-af3b6e838a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e1335bee-6ca3-483d-bece-35b4fd8dee71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f04565fb-1b6f-4bad-9c17-dec4e7b52d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 75,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2ffb243b-d756-4666-abfd-a8476eaf5e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b67a9a35-7ef2-4b2f-beab-51a8b845b167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 55,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "beed6fa0-0d9f-452f-847e-2fbc4ee0f12a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2c927550-4731-45c4-8bde-e8dbe0e020a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0dd9b0f7-6ade-4b2b-8455-a4de8b51801e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 34,
                "y": 98
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "890d66c9-9577-4b1b-851f-6390bfbe3075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 46,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Score: ",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}