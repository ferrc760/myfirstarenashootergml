{
    "id": "d483cf49-5cb5-4ac6-b175-e3c07380dd4a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9262b595-0d9d-41fa-bd3d-3b8db2e0244f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3ca12b5f-c8ca-4739-9fe7-8f55f2275e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 102,
                "y": 80
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d91ce0c0-5d7b-47b1-af75-a22bd2b66ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 90,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e4434cf4-8c9b-45c5-86e3-4c727a68c129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": -1,
                "shift": 23,
                "w": 24,
                "x": 64,
                "y": 80
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8ecc009c-31ee-49db-b608-4bd06708add8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 45,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "aa6b6077-e8d6-4efa-8deb-e7d476aa2272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 22,
                "y": 80
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ed735c1e-d511-4f81-b072-84e3b98c0a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "74942ad3-f539-4b0b-a1bf-9e40463dfffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 498,
                "y": 41
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f1fced7c-9711-4641-88f5-5b529a76662a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 486,
                "y": 41
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d699f448-c5e7-4172-b356-36054631db0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 474,
                "y": 41
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9cfee85f-df32-4868-8ac0-36be1cd6f705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 108,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "884b3bb7-0ae7-422a-8111-056bb88e2a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 459,
                "y": 41
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0ccc9c8b-4b4b-41d4-a24e-d0b7a94ecdb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 434,
                "y": 41
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6b134932-0242-4e0a-914e-d41f242b6a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 420,
                "y": 41
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9a157c8a-018c-46b3-baa1-94649e43d98e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 413,
                "y": 41
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "65fcdf5e-fd2f-4201-a9dd-9f7dd7e8d60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 397,
                "y": 41
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6f4c8551-676f-455b-8df9-c610845cee2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 378,
                "y": 41
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5f31fbb5-04e6-4fa8-8ec9-6c817437dba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 366,
                "y": 41
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c447ccd0-cc76-479b-98a9-1406da147384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 349,
                "y": 41
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "4b722b46-34b7-4261-859a-d2c489ac0d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 333,
                "y": 41
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3268ab20-afba-4b0a-891e-b1f107968b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 314,
                "y": 41
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f3cc1f4e-dcd7-4e77-8e0f-81c737478ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 442,
                "y": 41
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "107e97cd-fe9a-4ae2-ba11-15f205c523b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 124,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "84fa0cb2-65b7-46c5-898c-a8b6e379ae85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 141,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "134309fa-7706-42a7-a1aa-ab1ce489e254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 160,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "18a80fd5-aaee-48a4-8f0e-34c2b4598596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 19,
                "y": 119
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "53f22ca4-3926-4b23-901a-a5e3ca8ab8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 11,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac9d6fb8-2881-47d9-9a8b-72bb71d6ddb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d531ab21-5b88-4647-a2cc-cf26a3a88746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 498,
                "y": 80
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8ff9afb1-181e-4633-a048-133b5721dbe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 484,
                "y": 80
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "57b2bbe2-e9fa-4b0c-9093-fe3ba2d6e591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 11,
                "x": 471,
                "y": 80
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a9612f5e-5bf8-4147-a3ad-d10bd238ba3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 455,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1f4e1cc4-cb02-4e03-9619-3e3a37c28fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 429,
                "y": 80
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d6b04fba-0854-451d-97c8-7d4d09e5c3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 409,
                "y": 80
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a02dfc5d-febb-412e-8f1d-a78ef37e6067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 391,
                "y": 80
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9aa84ae3-6e76-41a3-928e-2f94bd220518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 372,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "737e6439-03ed-4c7e-8709-5334a5a8a985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 352,
                "y": 80
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6e0ec164-2210-4647-83b5-c786b81803d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 334,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f4215272-483c-4e37-90f2-b875b0feab15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 317,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "93966b5b-c866-4470-89c4-05fe67f8fc3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 296,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "902f1212-bc69-4412-bf47-b8352d408b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 275,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fac17542-cbc7-4d6c-9d6a-0fac64822749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 258,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ba44fa8c-e088-4da5-a152-af075a4ee1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 238,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2f843498-1222-4cde-a27c-77ebf128d22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 220,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0fa0b44e-7d73-4785-8827-6502337300a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 203,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4fd4a6a7-f976-44bc-8073-ec5131d4a4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 177,
                "y": 80
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2ac0dbea-9cec-404f-a405-64d5f14e0553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 292,
                "y": 41
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "53c024e6-610e-4f62-8a81-86ccc2d8e395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 269,
                "y": 41
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a560d76a-1ddd-4510-8a1b-4b69285b60b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 253,
                "y": 41
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b7e1669e-9187-4e7a-b0ad-4e995599a713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4f261526-c2df-4df6-a99e-2ea8b3388a5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 359,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "01cac5fe-eeff-48ad-8589-18a5f8229e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 338,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5468a60e-6608-4e59-8eab-734b1e05bf97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "53886c4f-0c67-4f46-a5d2-c6124aca49d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a691f94b-8a54-4a4f-85fb-a1454fe9b60b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 277,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3756f93c-93fd-4a33-8ac9-d56efe6aba7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4920ad27-5c1d-46df-a8e4-f41e6578a482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "826d9988-8263-4f3b-b42e-765317707a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "24e3cb41-3440-4359-a3aa-82d6dc6b8ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "699c2714-a9ac-4000-a412-a389d8b7b9b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d6192ecb-5a68-4be4-a5f7-f1f5d46e0587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "59a67f1a-8fd3-4feb-a962-b648c7218fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ef549b30-4491-4b0e-8960-1e86dbfed824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4cb13f2c-355f-4eb5-8d48-72abfc55020e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ec34fb93-4c7d-415b-a463-99b03e8d45cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 8,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "aa7c9ca7-d610-45db-be44-a5464b34ff53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f777b6b9-422e-4ee5-a3c8-868744548fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ebcd6496-e3a2-41e4-9528-c7e6dadec6da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0deede77-dbc2-49d5-a8d3-c608401b1844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "44c12788-e314-4db8-ac28-a10a9ae1e8af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9e28539a-2d20-4346-9cad-1cb2659a556a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "09ef0817-760a-4d42-883b-f6800f88d6df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f2527083-4589-4a10-a8a2-4b5b9abe57fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 73,
                "y": 41
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "24a177d4-4b45-478f-a5fe-d4f4003ed6d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 430,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7afedc8c-40de-4c89-989d-87cd2577b47f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 225,
                "y": 41
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8f75494a-d998-4223-b4c4-e1dc578edc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 209,
                "y": 41
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "94da99f6-ab5b-48a0-a52e-9cb4ac29dd06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 202,
                "y": 41
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "97111157-e5da-4624-8e0d-6ee7e37418d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 180,
                "y": 41
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2554fc51-ffac-4b93-8255-4b7bdd7d6d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 164,
                "y": 41
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "94337d12-86d2-446b-bc13-a0ff25e07e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 148,
                "y": 41
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2271c103-040f-48de-896b-15f4bc41c28c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 133,
                "y": 41
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4fe25623-6bd9-438d-b3ee-d1c4ae39c29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 118,
                "y": 41
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f9449e93-7480-46bb-8cda-681547df2055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 104,
                "y": 41
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1c78009c-7189-4609-aee7-b0eefd535d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 238,
                "y": 41
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e8fe17da-5067-4881-8afb-dc8d4e2794ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 89,
                "y": 41
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "440a8656-b9fc-46e1-b5bf-0a61aeed578d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 57,
                "y": 41
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ee5f5a95-bd25-465c-a178-13a4208622f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 41,
                "y": 41
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3fc7987f-78b9-4e9f-b5c1-ba213191a807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 20,
                "y": 41
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6222418b-f9db-4e2d-9dbd-9de7adab6ef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a4b0b6d4-67ba-43f0-9667-704586013934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c6289d17-9656-4373-8cc1-2279a2efa43b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "24cb128a-52da-4baf-859c-7cfa8b93cad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9d6615f2-78e7-47ba-938a-60cc90973e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 451,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b50bb081-1392-4b2e-a112-86240b35f1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 438,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "68771a83-2b74-4b7e-bf37-f6a6e91c63bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 37,
                "y": 119
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ba4338f0-ddcf-4514-9f87-2872c67d9a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 37,
                "offset": 5,
                "shift": 26,
                "w": 16,
                "x": 55,
                "y": 119
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "Score: ",
    "size": 20,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}