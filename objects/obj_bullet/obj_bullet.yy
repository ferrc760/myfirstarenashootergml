{
    "id": "1d763127-ed4c-46f1-82ba-f885797217d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "d6d6b4a8-bfb3-482f-b09c-e8ab09283073",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d763127-ed4c-46f1-82ba-f885797217d2"
        },
        {
            "id": "4d9e8295-cb38-41fa-82b7-a5ebcff656dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6ae1ff91-de12-4fdd-b5aa-fef7fe736546",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1d763127-ed4c-46f1-82ba-f885797217d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2111afa-ef58-48ad-8925-e742ca155a30",
    "visible": true
}