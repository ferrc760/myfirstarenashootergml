/// @description Insert description here
// You can write your code in this editor
if (instance_exists(obj_player))
{
    move_towards_point(obj_player.x, obj_player.y, spd);
}
powerup = random_range(1, 6);
image_angle = direction; 

if (hp) <= 0 
{
  if (powerup) >= 5
  {
  instance_create_layer(x,y, "Instances", obj_powerups)
  }
  with(obj_score) thescore = thescore + 5;
   audio_sound_pitch(snd_death, random_range(0.8, 1.2));
audio_play_sound(snd_death, 1, false);
instance_create_layer(x,y,"Enemy_splat", obj_enemydeath);
   instance_destroy();
}