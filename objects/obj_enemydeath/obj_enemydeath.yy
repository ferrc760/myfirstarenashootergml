{
    "id": "357aa18e-97fd-43b9-902e-021dd00d4085",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemydeath",
    "eventList": [
        {
            "id": "1f64a4f4-1783-4e54-a611-08ee86083772",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "357aa18e-97fd-43b9-902e-021dd00d4085"
        },
        {
            "id": "2bfef916-3ba0-4d1d-979a-32cae7badd5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "357aa18e-97fd-43b9-902e-021dd00d4085"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73bae969-7fc1-4772-9bcf-bf25aa44829d",
    "visible": true
}