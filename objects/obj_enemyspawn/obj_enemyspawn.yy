{
    "id": "c7a8b527-e867-48aa-8f2b-f9b5fd0bda05",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemyspawn",
    "eventList": [
        {
            "id": "7eea3170-3288-473b-bad9-037ec269cb38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7a8b527-e867-48aa-8f2b-f9b5fd0bda05"
        },
        {
            "id": "4adcacad-7da0-4e4e-a2e8-ad6c48b66069",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c7a8b527-e867-48aa-8f2b-f9b5fd0bda05"
        },
        {
            "id": "203c8b9b-0b7a-4f06-a663-a84932d16b7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c7a8b527-e867-48aa-8f2b-f9b5fd0bda05"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}