/// @description Insert description here
// You can write your code in this editor
if (keyboard_check(ord("D"))) x += 4;
if (keyboard_check(ord("S"))) y += 4;
if (keyboard_check(ord("A"))) x -= 4;
if (keyboard_check(ord("W"))) y -= 4;

image_angle = point_direction(x, y, mouse_x, mouse_y);


if(defaultgun) = 1
{
if (mouse_check_button(mb_left)) && (cooldown < 1)
{
 instance_create_layer(x, y, "bullet_layer", obj_bullet);
 cooldown = 10;
 audio_play_sound(snd_shoot, 1, false);
 audio_stop_sound(snd_powerupshoot);
}

cooldown = cooldown - 1;
}
if (powerupgun) = 1
{
	audio_play_sound(snd_powerupshoot, 11, false);
	if(mouse_check_button(mb_left)) && (cooldown <1)
	{
		instance_create_layer(x,y,"bullet_layer", obj_bullet);
		cooldown = 3;
	}
	cooldown = cooldown - 1;
	 
}