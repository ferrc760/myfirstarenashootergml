{
    "id": "956cf60e-d3c2-4da6-ba1c-2a25c1192370",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "471c46a3-0f0c-4041-be97-7ffffed62eca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "956cf60e-d3c2-4da6-ba1c-2a25c1192370"
        },
        {
            "id": "b31b39c8-9aa6-4076-805e-a183f6de2a08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "956cf60e-d3c2-4da6-ba1c-2a25c1192370"
        },
        {
            "id": "3133391b-4b30-47bb-b207-57ae956ed47f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6ae1ff91-de12-4fdd-b5aa-fef7fe736546",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "956cf60e-d3c2-4da6-ba1c-2a25c1192370"
        },
        {
            "id": "7fc88ba1-b552-48a1-99b0-8d7e5c5bbe0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8b14e1f5-2dce-47f4-9001-b4732b455dbd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "956cf60e-d3c2-4da6-ba1c-2a25c1192370"
        },
        {
            "id": "47ae3d3a-3ba4-4db1-8c86-0b641a953326",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2a1939d9-d990-4de1-87b5-5d02bc712786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "956cf60e-d3c2-4da6-ba1c-2a25c1192370"
        },
        {
            "id": "cfbe5fa2-b96c-4ff9-abba-c5b8b19787cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "956cf60e-d3c2-4da6-ba1c-2a25c1192370"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24c0c086-7a43-44d7-b622-765ec0fb183b",
    "visible": true
}