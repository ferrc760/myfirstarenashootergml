{
    "id": "2a1939d9-d990-4de1-87b5-5d02bc712786",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_powerups",
    "eventList": [
        {
            "id": "d46a12df-333a-4c59-90f2-41dca6d57ea6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a1939d9-d990-4de1-87b5-5d02bc712786"
        },
        {
            "id": "8e3f18dd-4625-4257-a436-ec44c5c6de8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a1939d9-d990-4de1-87b5-5d02bc712786"
        },
        {
            "id": "1d5b78a5-4b14-4c17-95e8-f9c80873c47a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "956cf60e-d3c2-4da6-ba1c-2a25c1192370",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2a1939d9-d990-4de1-87b5-5d02bc712786"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "e595fdb5-2503-4cb4-a3b8-a84a721fb125",
    "visible": true
}