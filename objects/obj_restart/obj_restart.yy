{
    "id": "f35f8f2c-a643-49c3-ad65-e54e9f679735",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_restart",
    "eventList": [
        {
            "id": "72a5595c-5751-4797-b685-a0eac71a0158",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f35f8f2c-a643-49c3-ad65-e54e9f679735"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "04521bf2-c13b-468f-b9ac-f3121093a10d",
    "visible": true
}