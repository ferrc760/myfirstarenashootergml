{
    "id": "96da1dc8-9ed7-4b5a-87d1-7b117df45708",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title",
    "eventList": [
        {
            "id": "33cb6919-7cca-4e22-b3ae-4c819e1c68fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96da1dc8-9ed7-4b5a-87d1-7b117df45708"
        },
        {
            "id": "06481e89-343e-4eda-a448-d245709c3f34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "96da1dc8-9ed7-4b5a-87d1-7b117df45708"
        },
        {
            "id": "ab25aa6f-5845-4549-b42f-6128daf29c1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 10,
            "m_owner": "96da1dc8-9ed7-4b5a-87d1-7b117df45708"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0b2ffede-22e5-4f54-b4e3-ef54465742a9",
    "visible": true
}