{
    "id": "0f7c759f-bfe3-4e34-9832-ad3cfaa95948",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f8e0608-3f45-47be-9111-e48d8fe83954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f7c759f-bfe3-4e34-9832-ad3cfaa95948",
            "compositeImage": {
                "id": "1f5a3168-c8f3-42db-9328-6a2b55d963e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8e0608-3f45-47be-9111-e48d8fe83954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37dc367-4b45-4ecd-b08a-3d23ef181d25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8e0608-3f45-47be-9111-e48d8fe83954",
                    "LayerId": "c81b6b40-24b0-4ef5-9bc1-917a683e229d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "c81b6b40-24b0-4ef5-9bc1-917a683e229d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f7c759f-bfe3-4e34-9832-ad3cfaa95948",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}