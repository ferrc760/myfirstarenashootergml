{
    "id": "17d8f712-d0c2-421b-9b97-0d4be2b1207a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d60c5384-576e-4e02-8379-f2b06f1cdb67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17d8f712-d0c2-421b-9b97-0d4be2b1207a",
            "compositeImage": {
                "id": "8ddab331-0a9b-4258-bb18-7cfe3235d06b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d60c5384-576e-4e02-8379-f2b06f1cdb67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f45d55-f02f-488c-9727-355894e69b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d60c5384-576e-4e02-8379-f2b06f1cdb67",
                    "LayerId": "5125992f-63ad-4fc4-90a1-dd4a44248e6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5125992f-63ad-4fc4-90a1-dd4a44248e6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17d8f712-d0c2-421b-9b97-0d4be2b1207a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}