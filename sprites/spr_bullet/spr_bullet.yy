{
    "id": "b2111afa-ef58-48ad-8925-e742ca155a30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 92,
    "bbox_right": 110,
    "bbox_top": 53,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a1c0223-588f-4a69-92d1-8f16b8a38e0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2111afa-ef58-48ad-8925-e742ca155a30",
            "compositeImage": {
                "id": "6f68acbc-aef6-4286-b25e-0fabd6b361bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a1c0223-588f-4a69-92d1-8f16b8a38e0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1074a09c-c712-487e-9a2d-54753ac05000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a1c0223-588f-4a69-92d1-8f16b8a38e0e",
                    "LayerId": "8748c20b-8d6f-49e4-81e0-426c13abb89f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8748c20b-8d6f-49e4-81e0-426c13abb89f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2111afa-ef58-48ad-8925-e742ca155a30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 104,
    "yorig": 62
}