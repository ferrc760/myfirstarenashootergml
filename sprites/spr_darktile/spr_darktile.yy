{
    "id": "1db30e30-c93b-4380-9e84-c89a1bd9f27e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darktile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a80193af-f0cd-422d-8f13-9c16a49fc756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1db30e30-c93b-4380-9e84-c89a1bd9f27e",
            "compositeImage": {
                "id": "52948078-144e-40a2-ae9a-c15345fcca85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80193af-f0cd-422d-8f13-9c16a49fc756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b58c5c13-f29a-460d-aab3-e7a96df07288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80193af-f0cd-422d-8f13-9c16a49fc756",
                    "LayerId": "d4168a4d-1943-4607-8002-abb948831198"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d4168a4d-1943-4607-8002-abb948831198",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1db30e30-c93b-4380-9e84-c89a1bd9f27e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}