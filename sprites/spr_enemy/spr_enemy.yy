{
    "id": "cc4de77d-1719-4cee-94b6-5ee5ea988010",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 21,
    "bbox_right": 91,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a437d75d-48e0-4c85-8366-7ea422766e51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc4de77d-1719-4cee-94b6-5ee5ea988010",
            "compositeImage": {
                "id": "9815fa7b-af8a-4bb0-8016-6712ed4df1e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a437d75d-48e0-4c85-8366-7ea422766e51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85bc07c4-381b-4dcb-ab52-85d3552aadda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a437d75d-48e0-4c85-8366-7ea422766e51",
                    "LayerId": "a1309af0-f8bf-495e-ad16-02f864b6e77a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "a1309af0-f8bf-495e-ad16-02f864b6e77a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc4de77d-1719-4cee-94b6-5ee5ea988010",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}