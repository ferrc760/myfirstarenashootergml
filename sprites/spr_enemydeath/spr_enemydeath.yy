{
    "id": "73bae969-7fc1-4772-9bcf-bf25aa44829d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemydeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 13,
    "bbox_right": 103,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4955c8b6-6b35-4ec3-84f4-b068376e53f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73bae969-7fc1-4772-9bcf-bf25aa44829d",
            "compositeImage": {
                "id": "b501df01-57b9-44dc-af96-d690565c56f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4955c8b6-6b35-4ec3-84f4-b068376e53f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de814c17-6e39-49b6-8ac1-dad679cf5cbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4955c8b6-6b35-4ec3-84f4-b068376e53f4",
                    "LayerId": "b11fcff9-04ae-4c5e-ba13-b8293b114b8d"
                }
            ]
        },
        {
            "id": "65cc5010-8037-4932-ba8d-bb17f9f5f842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73bae969-7fc1-4772-9bcf-bf25aa44829d",
            "compositeImage": {
                "id": "da8ac4b8-3afa-4bbc-8d43-933bfeb61d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65cc5010-8037-4932-ba8d-bb17f9f5f842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a0658e-bb45-4637-9207-1ed487889b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65cc5010-8037-4932-ba8d-bb17f9f5f842",
                    "LayerId": "b11fcff9-04ae-4c5e-ba13-b8293b114b8d"
                }
            ]
        },
        {
            "id": "b92738d3-9d49-4146-b99c-464a7f68a182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73bae969-7fc1-4772-9bcf-bf25aa44829d",
            "compositeImage": {
                "id": "ff76b8b2-cdff-482c-8836-92ab132a3d07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92738d3-9d49-4146-b99c-464a7f68a182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c0e3e53-5298-47ba-b2b8-6e053fe67c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92738d3-9d49-4146-b99c-464a7f68a182",
                    "LayerId": "b11fcff9-04ae-4c5e-ba13-b8293b114b8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "b11fcff9-04ae-4c5e-ba13-b8293b114b8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73bae969-7fc1-4772-9bcf-bf25aa44829d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}