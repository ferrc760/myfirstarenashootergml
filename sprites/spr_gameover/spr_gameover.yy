{
    "id": "a9e34ac8-3330-4ce9-b32f-43a359cb7b64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a7e324d-69f0-4e65-b315-fcd0c0ea1294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e34ac8-3330-4ce9-b32f-43a359cb7b64",
            "compositeImage": {
                "id": "5be3ab35-bdb8-4243-9130-96eed90d49a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a7e324d-69f0-4e65-b315-fcd0c0ea1294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde010a2-1946-4c5e-b326-0e9efa3c8fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a7e324d-69f0-4e65-b315-fcd0c0ea1294",
                    "LayerId": "b45a28e4-2f0a-4fb1-921c-ce3f38024e74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b45a28e4-2f0a-4fb1-921c-ce3f38024e74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9e34ac8-3330-4ce9-b32f-43a359cb7b64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}