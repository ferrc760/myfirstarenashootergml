{
    "id": "24c0c086-7a43-44d7-b622-765ec0fb183b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 23,
    "bbox_right": 90,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5990fd1a-6d5a-4e28-b96b-7b770d3af4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24c0c086-7a43-44d7-b622-765ec0fb183b",
            "compositeImage": {
                "id": "8830a78c-f8da-410b-a3c2-7740cef50c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5990fd1a-6d5a-4e28-b96b-7b770d3af4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df0d0565-6ebe-4307-a6b7-9e53aa5e4fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5990fd1a-6d5a-4e28-b96b-7b770d3af4a3",
                    "LayerId": "1f43d1a0-5e17-4073-b81e-45b7b7495cd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "1f43d1a0-5e17-4073-b81e-45b7b7495cd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24c0c086-7a43-44d7-b622-765ec0fb183b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 57
}