{
    "id": "e595fdb5-2503-4cb4-a3b8-a84a721fb125",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_powerups",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 30,
    "bbox_right": 115,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b7977b4-2783-4dd7-9ece-7778602f0a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e595fdb5-2503-4cb4-a3b8-a84a721fb125",
            "compositeImage": {
                "id": "41fa4a3a-703d-4e3e-9602-85e012894f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7977b4-2783-4dd7-9ece-7778602f0a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482b5e2d-edb1-4a5c-b7fe-a4f30f016867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7977b4-2783-4dd7-9ece-7778602f0a5a",
                    "LayerId": "eb7f0eb0-9527-444d-8d26-b9bbbe5f53c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "eb7f0eb0-9527-444d-8d26-b9bbbe5f53c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e595fdb5-2503-4cb4-a3b8-a84a721fb125",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 99,
    "yorig": 48
}