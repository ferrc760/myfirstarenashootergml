{
    "id": "04521bf2-c13b-468f-b9ac-f3121093a10d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_restart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efefd8b8-ca3f-4d40-8cdc-8db1edbf4328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04521bf2-c13b-468f-b9ac-f3121093a10d",
            "compositeImage": {
                "id": "c6387bf0-5347-48d4-800b-23c50bf16e0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efefd8b8-ca3f-4d40-8cdc-8db1edbf4328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "481fa82f-4402-4908-88d6-4ca25d2beb8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efefd8b8-ca3f-4d40-8cdc-8db1edbf4328",
                    "LayerId": "21473c45-efff-49a4-8c67-bf17fdc13e39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "21473c45-efff-49a4-8c67-bf17fdc13e39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04521bf2-c13b-468f-b9ac-f3121093a10d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}