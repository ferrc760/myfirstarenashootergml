{
    "id": "0b2ffede-22e5-4f54-b4e3-ef54465742a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titlescreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 282,
    "bbox_left": 116,
    "bbox_right": 781,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6686054-a6c1-41bf-80aa-d90a41c7bf69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b2ffede-22e5-4f54-b4e3-ef54465742a9",
            "compositeImage": {
                "id": "55455f70-f06a-487f-865d-cda3bacbb383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6686054-a6c1-41bf-80aa-d90a41c7bf69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84cc08c2-e387-4738-aeab-e97fa7aa1b5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6686054-a6c1-41bf-80aa-d90a41c7bf69",
                    "LayerId": "75d91137-44db-4abb-92c8-69a976e72634"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "75d91137-44db-4abb-92c8-69a976e72634",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b2ffede-22e5-4f54-b4e3-ef54465742a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 860,
    "xorig": 430,
    "yorig": 141
}